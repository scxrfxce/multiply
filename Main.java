
import java.util.Scanner;

public class Main {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int summ;
        boolean isNegative = false;
        int[] parsed = new int[2];
        parsed = Input(parsed);
        if (parsed[0] == 0 || parsed[1] == 0) {
            summ = 0;
        } else {
            parsed = Swap(parsed);

            if (parsed[1] < 0) {
                parsed[1] *= -1;
                isNegative = true;
            }

            summ = Multiply(parsed);

            if (isNegative) {
                summ *= -1;
            }
        }

        System.out.println("Конечный результат - " + summ);
    }

    public static int[] Input(int[] parsed) {
        System.out.println("Введите умножаемую строку");
        String arguments = sc.nextLine();
        String[] splitted = arguments.split("\\*");
        for (int i = 0; i < splitted.length; i++) parsed[i] = Integer.parseInt(splitted[i]);

        return parsed;
    }

    public static int[] Swap(int[] parsed) {
        if (parsed[0] < parsed[1]) {
            int storage = parsed[1];
            parsed[1] = parsed[0];
            parsed[0] = storage;
        }
        return parsed;
    }

    public static int Multiply(int[] parsed) {
        int summ = parsed[0];
        for (int j = 1; j < parsed[1]; j++) {
            summ = summ + summ / j;
        }
        return summ;
    }
}
